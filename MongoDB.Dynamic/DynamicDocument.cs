using System;
using MongoDB.Bson;

namespace MongoDB.Dynamic
{
    //[BsonIgnoreExtraElements(Inherited = true)]
    internal class DynamicDocument : BsonDocument
    {
        public DynamicDocument()
            : this(null, false)
        {
        }

        public DynamicDocument(object value, bool audit)
        {
            if (value != null)
                this["_id"] = BsonValue.Create(value);
            else this["_id"] = BsonNull.Value;

            if (audit)
                LastChanges = DateTime.Now;
        }

        public DateTime LastChanges
        {
            get { return this["LastChanges"].AsDateTime; }
            set { this["LastChanges"] = value; }
        }


    }
}