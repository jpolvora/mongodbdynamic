﻿// Criado por Jone Polvora
// 02 06 2012

using System.Collections.Generic;

namespace MongoDynamic.Tests
{
    public interface ICustomer
    {
        int Id { get; set; }
        string Name { get; set; }

        IEnumerable<IOrder> Orders { get; set; }
    }

    public interface IOrder
    {
        int Id { get; set; }
        ICustomer Customer { get; set; }
        int IdCustomer { get; set; }
    }
}