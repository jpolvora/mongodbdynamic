namespace MongoDB.Dynamic
{
    internal static class Extensions
    {
        public static object GetKey(this DynamicDocument document)
        {
            return document["_id"].RawValue;
        }
    }
}