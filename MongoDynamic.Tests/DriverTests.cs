﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB;
using MongoDB.Driver;
using MongoDB.Dynamic;
using MongoDynamic.Tests.Driver;

namespace MongoDynamic.Tests
{
    [TestClass]
    public class DriverTests
    {
        private static readonly MongoCollection<Customer> Customers = Dynamic.Db.GetCollection<Customer>("Customers");
        private static readonly MongoCollection Keys = Dynamic.Db.GetCollection("MongoKeys");

        private const long Records = 1000;

        [ClassInitialize]
        public static void Initialize(TestContext ctx)
        {
            Customers.Drop();
            Keys.Drop();
            Assert.IsTrue(Customers.Count() == 0);
        }

        [TestMethod]
        public void DrvInserts()
        {
            for (int i = 0; i < Records; i++)
            {
                var customer = new Customer { Name = "The Jones" };
                Customers.Insert(customer);
            }
            Assert.IsTrue(Customers.Count() == Records);
        }

        [TestMethod]
        public void DrvFindAndUpdate()
        {
            for (int i = 1; i <= Records; i++)
            {
                var customer = Customers.FindOneById(i);
                customer.Name = "The Jones 2";
                Customers.Save(customer);
            }
            Assert.IsTrue(Customers.Count() == Records);
        }

        [TestMethod]
        public void DrvUpdateByKey()
        {
            for (int i = 1; i <= Records; i++)
            {
                var customer = new Customer { Id = i, Name = "The Jones 3" };
                Customers.Save(customer);
            }
            Assert.IsTrue(Customers.Count() == Records);
        }
    }
}
