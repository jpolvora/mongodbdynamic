﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson.Serialization.Attributes;

namespace MongoDynamic.Tests.Driver
{
    public class Customer : ICustomer
    {
        [BsonId(IdGenerator = typeof(IdGenerator))]
        public int Id { get; set; }
        public string Name { get; set; }

        [BsonIgnore]
        public string ExtraIgnoredField { get; set; }

        public IEnumerable<IOrder> Orders { get; set; }
    }
}
