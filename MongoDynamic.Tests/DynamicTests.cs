using System.Dynamic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB;
using MongoDB.Dynamic;

namespace MongoDynamic.Tests
{
    [TestClass]
    public class DynamicTests
    {
        private static readonly DynamicCollection<ICustomer> Customers = Dynamic.GetCollection<ICustomer>();
        private const long Records = 1000;

        [ClassInitialize]
        public static void Initialize(TestContext ctx)
        {
            Dynamic.Config.SetKeyName<ICustomer>(customer => customer.Id);
            Customers.RemoveAll(true);
            Assert.IsTrue(Customers.Count() == 0);
        }

        [TestMethod]
        public void DynInserts()
        {
            for (int i = 0; i < Records; i++)
            {
                var customer = Customers.New();
                customer.Name = "The Jones";
                Customers.Upsert(customer);
            }
            Assert.IsTrue(Customers.Count() == Records);
        }

        [TestMethod]
        public void DynFindAndUpdate()
        {
            for (int i = 1; i <= Records; i++)
            {
                var customer = Customers.GetByKey(i);
                customer.Name = "The Jones 2";
                Customers.Upsert(customer);
            }
            Assert.IsTrue(Customers.Count() == Records);
        }

        [TestMethod]
        public void DynUpdateByKey()
        {
            for (int i = 1; i <= Records; i++)
            {
                dynamic customer = new ExpandoObject();
                customer.Id = i;
                customer.Name = "The Jones 3";
                Customers.UpsertImpromptu(customer);
            }
            Assert.IsTrue(Customers.Count() == Records);
        }

    }
}